---
title: "News"
layout: textlay
excerpt: "van der Sar Lab at TU Delft."
sitemap: false
permalink: /allnews.html
---

# News

{% for article in site.data.news %}
<b>{{ article.date }}</b>
{{ article.headline | markdownify}}
{% endfor %}
