---
title: "van der Sar Lab - Research"
layout: textlay
excerpt: "van der Sar Lab -- Research"
sitemap: false
permalink: /research/
---

# Research
Our group studies charge- and spin-transport in materials that are both fundamentally intriguing and potentially useful for devices. We are particularly interested in the collective behaviour that arises from particle interactions. The vision is to realize a set of nanoscale imaging and electronic-transport tools that enable new ways to address this topic. Along these lines, we have developed an new technique for imaging collective spin excitations known as spin waves (more below).
  
A central role in our lab is played by nitrogen-vacancy (NV) centers in diamond, which we use as magnetic-field sensors. This involves photoluminescence microscopy with single-photon sensitivity, single-spin microwave control, electronic transport, and scanning-probe microscopy. With these techniques we pursue the vision described above, focusing on two main research directions:

-	Spin-wave transport in thin-film magnets (largest effort)
-	Imaging electrical currents in quantum materials (taking off in the context of our NWO VIDI and NWA projects)

**Spin waves**
Spin waves are the elementary excitations of magnetic materials. A growing research field is focusing on creating spin-wave technology, such as spin-wave transistors. Theory is addressing fundamental questions about nanoscale spin-wave transport, the interaction of spin waves with other excitations such as phonons, adding excitement by predicting exotic phenomena such as spin superfluidity and the spin-wave quantum-Hall effect. Being able to probe and control spin-wave transport with high resolution at buried magnetic interfaces is key to realizing these exciting prospects. Check out our publications page for the latest results!

![]({{ site.url }}{{ site.baseurl }}/images/picpic/NVaboveSpinwaveConduct.png){: style="width: 70%; float: center; margin: 0px"}

