---
title: "van der Sar Lab - Vacancies"
layout: textlay
excerpt: "Openings"
sitemap: false
permalink: /openings/
---

# Open positions

## Open PhD position on magnetic imaging of spin waves and superconductors using spins in diamond.
We have an open PhD position. Please see a full description and the way to apply [here](https://careers.tudelft.nl/job/Delft-PhD-Position-Magnetic-Imaging-of-Spin-Waves-and-Superconductors-Using-Spins-in-Diamond-2628-CD/815291302/). In a nutshell:

#### Topic

Spin waves could enable information technology with new functionalities due to their microwave frequencies, strong interactions, and low intrinsic dissipation. A key goal is to realize efficient spin-wave control. In this project, you will develop superconductor-based control of spin waves in the context of an [ERC Consolidator Program](https://www.tudelft.nl/en/2024/tu-delft/four-erc-consolidator-grants-for-tu-delft-researchers). The goal is to study the interaction between magnetism and superconductivity with high spatial resolution using a cryogenic, scanning-NV magnetic imaging system, and thereby develop new ways to excite, guide, and detect spin waves. 

#### Requirements:

The project revolves around NV centers in diamond, single-spin control, magnetization dynamics, superconductivity, single-photon and atomic-force microscopy, microwave engineering, cryogenic measurements, cleanroom fabrication, and electronic characterization. Experience in these fields is highly desirable, though not strictly required. Most importantly, you should be highly enthusiastic and motivated to develop state-of-the-art scanning-NV microscopy. A MSc degree in physics and fluency in English are required.

#### Location

The van der Sar Lab is part of the department of Quantum Nanoscience at the Kavli Institute of Nanoscience at Delft University of Technology. Delft is a university town close to the larger cities of The Hague and Rotterdam (both ~15 min. by train) and Amsterdam (~ an hour by train). 
When and how to apply
Please apply via [this link](https://careers.tudelft.nl/job/Delft-PhD-Position-Magnetic-Imaging-of-Spin-Waves-and-Superconductors-Using-Spins-in-Diamond-2628-CD/815291302/), including :
1.	A cover letter, including (1) a brief personal introduction, (2) an explanation of how your previous studies and experience have prepared you for this PhD position, and (3) why you are interested in this position and our lab. The maximum length is one page. 
2.	Detailed CV.
3.	Copies of your BSc and MSc degrees and transcripts. 
4.	Names and contact information of at least two people we can contact for reference.

## General information 

For postdoc funding: Please take a look at the [Veni fellowship](https://www.nwo.nl/en/calls/nwo-talent-programme-veni-science-domain-2023) or the Marie Curie fellowship, [here is a previous call]({{ site.baseurl }}/downloads/h2020-wp1820-msca_en.pdf)). In many countries, there are fellowships available for outgoing postdocs.** I can help writing the application. Our group currently hosts a Marie-Curie and a NWO-technology fellow (see team).

## Master/bachelor projects for TU Delft students
For information on possible MSc or BSc thesis projects, please contact Toeno at t.vandersar@tudelft.nl.

Note that Masters and Bachelors projects are only available to students already enrolled in a Bachelors or Masters program at a university in the Netherlands. For information about enrolling for the Bachelors or Masters program at TU Delft, please consult the TU-Delft homepage.

<figure>
<img src="{{ site.url }}{{ site.baseurl }}/images/picpic/Gallery/MScAP_BrochurePicture.PNG" width="80%">
</figure>
